resource "aws_vpc" "vpc_datadog" {
  cidr_block       = "10.10.0.0/16"
 
}
resource "aws_subnet" "subnet-2" {
  vpc_id      = aws_vpc.vpc_datadog.id
  availability_zone = var.subnet2az
  cidr_block        = "10.10.0.0/24"
  
}

resource "aws_internet_gateway" "my_igw" {
    vpc_id      = aws_vpc.vpc_datadog.id
   
}

resource "aws_route_table" "my_rt" {
    vpc_id = aws_vpc.vpc_datadog.id
    route {
        cidr_block  = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.my_igw.id
    }
 
}

resource "aws_security_group" "my_sg" {
     
    vpc_id          = aws_vpc.vpc_datadog.id
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "tcp"
        from_port   = "22"
        to_port     = "22"
    }
    
    ingress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "UDP"
        from_port   = "8125"
        to_port     = "8125"
    }
    
    egress{
        cidr_blocks = ["0.0.0.0/0"]
        protocol    = "-1"
        from_port   = "0"
        to_port     = "0"
    }
}

resource "aws_route_table_association" "subnet2assoc" {
    subnet_id       = aws_subnet.subnet-2.id
    route_table_id  = aws_route_table.my_rt.id
  
}


resource "aws_instance" "Datadog" {
    ami                         =  var.datadog
    instance_type               = "t2.micro"
    subnet_id                   = aws_subnet.subnet-2.id
    associate_public_ip_address = true
    vpc_security_group_ids      = [aws_security_group.my_sg.id]
    key_name                    = var.awskeypair
   
     connection {
        type        = "ssh"
        user        = var.sshusername
        private_key = file(var.sshkeypath)
        host        = aws_instance.Datadog.public_ip
    }

     provisioner "remote-exec" {
    inline = [
        
          "sudo apt-get -y update",
    #  "sudo DD_AGENT_MAJOR_VERSION=7 DD_API_KEY=c2b42497f64cc74409b43a709bdb9505 bash -c '$(curl -L https://raw.githubusercontent.com/DataDog/datadog-agent/master/cmd/agent/install_script.sh)'",
     #   "curl 'https://api.datadoghq.com/api/v1/validate' / -H 'DD-API-KEY: c2b42497f64cc74409b43a709bdb9505' / -H 'DD-APPLICATION-KEY: 53f7e9cced0a27821c51e04b32c16576373925fa'",

         # to download the datadog package
        "sudo DD_API_KEY=c2b42497f64cc74409b43a709bdb9505 bash -c '$(curl -L https://raw.githubusercontent.com/DataDog/dd-agent/master/packaging/datadog-agent/source/install_agent.sh)'",
         #"sudo service datadog-agent start",
         "sudo initctl start datadog-agent",
     #   "sudo /etc/init.d/datadog-agent stop",
       # "sudo /etc/init.d/datadog-agent start",
       "DD_INSTALL_ONLY=true DD_API_KEY=c2b42497f64cc74409b43a709bdb9505 bash -c '$(curl -L https://raw.githubusercontent.com/DataDog/dd-agent/master/packaging/datadog-agent/source/install_agent.sh)'",
      #  "sudo invoke-rc.d datadog-agent restart  ",
        #"sudo /etc/init.d/datadog-agent restart -y",
        "sudo initctl start datadog-agent",
        "sudo initctl status datadog-agent"
         
         
    ]
  }
  
}
output "vpc_datadog_ip" {
  value = aws_instance.Datadog.public_ip
}

